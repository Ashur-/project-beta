from django.db import models
from django.urls import reverse
from datetime import datetime


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, default="")
    sold = models.BooleanField(default=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=50, default="")
    last_name = models.CharField(max_length=50, default="")
    employee_id = models.PositiveSmallIntegerField(unique=True, default="0")

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})

    def to_dict(self):
        return {
            "id": self.id,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "employee_id": self.employee_id,
        }


class Appointment(models.Model):
    date_time = models.DateTimeField(default=datetime.now)
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=20, default="pending")
    vin = models.CharField(max_length=17, default="")
    vip = models.BooleanField(default=False)
    customer = models.CharField(max_length=100, default="")
    technician = models.ForeignKey(
        Technician, related_name="appointments", on_delete=models.CASCADE
    )

    def canceled(self):
        self.status = "canceled"
        self.save()

    def finished(self):
        self.status = "finished"
        self.save()

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.id})
