from django.urls import path
from .views import (
    delete_customer,
    api_salesperson_list,
    delete_salesperson,
    api_sale_list,
    api_customer,
    delete_sale,
)


urlpatterns = [
    path("customer/", api_customer, name="api_customer"),
    path("customers/<int:pk>/", delete_customer, name="delete_customer"),
    path("salespeople/", api_salesperson_list, name="api_salesperson_list"),
    path("salespeople/<int:pk>/", delete_salesperson, name="delete_salesperson"),
    path("sales/", api_sale_list, name="api_sales"),
    path("sales/<int:pk>", delete_sale, name="delete_sale"),
]
