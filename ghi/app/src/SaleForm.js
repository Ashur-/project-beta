import React, { useState, useEffect } from "react"

function CreateSale() {
    const [automobiles, setAutomobiles] = useState([])
    const [automobile, setAutomobile] = useState('')
    const [salespeople, setSalespersons] = useState([])
    const [salesperson, setSalesperson] = useState('')
    const [customers, setCustomers] = useState([])
    const [customer, setCustomer] = useState('')
    const [price, setPrice] = useState('')



    const handleAutomobileChange = (event) => {
        const value = event.target.value
        setAutomobile(value)
    }
    const handleSalespersonChange = (event) => {
        const value = event.target.value
        setSalesperson(value)
    }
    const handleCustomerChange = (event) => {
        const value = event.target.value
        setCustomer(value)
    }
    const handlePriceChange = (event) => {
        const value = event.target.value
        setPrice(value)
    }

    const fetchData = async () => {
        const url = "http://localhost:8100/api/automobiles/"
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setAutomobiles(data.autos)
        }
    }
    const salespersonFetch = async () => {
        const url = "http://localhost:8090/api/salesperson/"
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setSalespersons(data.salesperson)
        }
    }
    const customersFetch = async () => {
        const url = "http://localhost:8090/api/customer/"
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setCustomers(data.customers)
        }
    }
    useEffect(() => {
        customersFetch()
        fetchData()
        salespersonFetch()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}

        data.automobile = automobile
        data.salespeople = salesperson
        data.customer = customer
        data.price = price

        const salesUrl = "http://localhost:8090/api/sales/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(salesUrl, fetchConfig)
        if (response.ok) {

            const automobileUrl = `http://localhost:8100/api/automobile`
            const automobileData = { sold: true }
            const FetchOptions = {
                method: "PUT",
                body: JSON.stringify(automobileData),
                headers: {
                    "Content-Type": "application/json",
                }
            }
            const automobileResponse = await fetch(automobileUrl, FetchOptions)
            if (automobileResponse.ok) {

                setAutomobile('')
                setSalesperson('')
                setCustomer('')
                setPrice('')
            }
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record A Sale</h1>
                    <form onSubmit={handleSubmit} id="create-sale-form">
                        <div className="mb-3">Automobile
                            <select onChange={handleAutomobileChange} value={automobile} required name="automobile" id="automobile" className="form-select">
                                <option value="">Choose an automobile VIN...</option>
                                {automobiles?.map(automobile => {
                                    if (automobile.sold === false) {
                                        return (
                                            <option key={automobile.id} value={automobile.vin}>
                                                {automobile.vin}
                                            </option>
                                        )
                                    }
                                })}
                            </select>
                        </div>
                        <div className="mb-3"> Salesperson
                            <select value={salesperson} onChange={handleSalespersonChange} required id="salesprson" name="salesperson" className="form-select">
                                <option value='' >Choose a salesperson...</option>
                                {salespeople.map(salesperson => {
                                    return (
                                        <option key={salesperson.id} value={salesperson.last_name} >
                                            {salesperson.first_name} {salesperson.last_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="mb-3"> Customer
                            <select value={customer} onChange={handleCustomerChange} required id="customer" name="customer" className="form-select">
                                <option value=''>Choose a customer...</option>
                                {customers.map(customer => {
                                    return (
                                        <option key={customer.id} value={customer.last_name}>
                                            {customer.first_name} {customer.last_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3"> Price
                            <input value={price} onChange={handlePriceChange} required type="text" id="price" name="price" className="form-control" />
                            <label htmlFor="Price"></label>
                        </div>
                        <button className="btn btn-primary me-md-4">Create</button>
                    </form>
                </div>
            </div>
        </div>

    )

}

export default CreateSale
