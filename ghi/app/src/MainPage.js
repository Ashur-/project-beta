import React from 'react';
import styles from './mainpage.module.css';


const MainPage = () => {
  return (
    <div className={styles.container}>
      <header className={styles.header}>
        <h1 style={{ color: "#0047ab", fontFamily: "Playfair Display, serif" }} className={styles.title}>CarCar</h1>
        <p className={styles.message}>
          The premiere solution for automobile dealership management!
        </p>
      </header>
      <div className={styles.mainpage}>
      </div>
    </div>
  );
}

export default MainPage;
