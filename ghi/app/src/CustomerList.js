import React, { useState, useEffect } from 'react';

function CustomerList() {
    const [customers, setCustomer] = useState([]);


    useEffect(() => {
        const fetchCustomer = async () => {
            try {
                const response = await fetch('http://localhost:8090/api/customer');
                if (!response.ok) {
                    throw new Error("Error");
                }
                const data = await response.json();
                setCustomer(data.customers)
                console.log(data.customers)
            } catch (error) {
                console.error(error);
            }
        };
        fetchCustomer();
    }, []);


    return (
        <>
            <h1>Customers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Fisrt Name</th>
                        <th>Last Name</th>
                        <th>Phone Number</th>
                        <th>Address</th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map((each) => (
                        <tr key={each.id}>
                            <td>{each.first_name}</td>
                            <td>{each.last_name}</td>
                            <td>{each.phone_number}</td>
                            <td>{each.address}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </>
    );
}

export default CustomerList;

