import React, { useState, useEffect } from 'react';

function SaleHistory() {
    const [customers, setCustomer] = useState([]);


    useEffect(() => {
        const fetchSale = async () => {
            try {
                const response = await fetch('http://localhost:8090/api/saleshistory');
                if (!response.ok) {
                    throw new Error("Error");
                }
                const data = await response.json();
                setCustomer(data.sales)
                console.log(data.sales)
            } catch (error) {
                console.error(error);
            }
        };
        fetchSale();
    }, []);


    return (
        <>
            <h1>Salesperson History</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Employee ID</th>
                        <th>Salesperson Name</th>
                        <th>Customer Name</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map((each) => (
                        <tr key={each.id}>
                            <td>{each.salesperson.employee_id}</td>
                            <td>{each.salesperson.first_name} {each.salesperson.last_name} </td>
                            <td>{each.customer.first_name} {each.customer.last_name}</td>
                            <td>{each.automobile.vin}</td>
                            <td>{each.price}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </>
    );
}

export default SaleHistory;

